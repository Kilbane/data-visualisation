# Data Visualisation
Coursework for the Year 5 module Data Visualisation taught by Dr. John Dingliana.

## Exercises
- 1.2: [Napoleon's Russian Campaign](assignment_1/assignment_1.2/minard)
- 1.3: [World Population](assignment_1/assignment_1.3/gapminder)
- 3: [Eurovision](assignment_3/eurovision)
