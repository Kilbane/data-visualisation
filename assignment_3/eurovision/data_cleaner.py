import json

f = open('data/contestants.json')
contestants = json.load(f)
f = open('data/votes.json')
votes = json.load(f)
f.close()

data = {}
for year in range(1957, 2020):
    data[year] = {}
    data[year]["final"] = {}
    data[year]["final"]["nodes"] = []
    data[year]["final"]["links"] = []
    data[year]["semi_final_1"] = {}
    data[year]["semi_final_1"]["nodes"] = []
    data[year]["semi_final_1"]["links"] = []
    data[year]["semi_final_2"] = {}
    data[year]["semi_final_2"]["nodes"] = []
    data[year]["semi_final_2"]["links"] = []

for contestant in contestants:
    node = {"id": contestant["to_country_id"]}
    data[contestant["year"]]["final"]["nodes"].append(node)
    if contestant["sf_num"] == 0:
        data[contestant["year"]]["semi_final_1"]["nodes"].append(node)
    elif contestant["sf_num"] == 1:
        data[contestant["year"]]["semi_final_1"]["nodes"].append(node)
    elif contestant["sf_num"] == 2:
        data[contestant["year"]]["semi_final_2"]["nodes"].append(node)

for vote in votes:
    link = {
             "source": vote["from_country_id"],
             "target": vote["to_country_id"],
             "total_points": vote["total_points"],
             "tele_points": vote["tele_points"],
             "jury_points": vote["jury_points"]
           }
    if vote["round"] == "final":
        data[vote["year"]]["final"]["links"].append(link)
    elif vote["round"] == "semi-final":
        data[vote["year"]]["semi_final_1"]["links"].append(link)
    elif vote["round"] == "semi-final-1":
        data[vote["year"]]["semi_final_1"]["links"].append(link)
    elif vote["round"] == "semi-final-2":
        data[vote["year"]]["semi_final_2"]["links"].append(link)
    
out_file = open('data/data.json', 'w')
json.dump(data, out_file, indent = 2)
