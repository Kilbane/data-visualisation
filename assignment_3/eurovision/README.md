# Eurovision Data Visualisation
The aim of this assignment was to create a visualisation of a complex dataset
that combined multiple visualisation idioms. I decided to visualise the jury
vote and televote distributions of the 42 countries that competed in the 2016
Eurovision Song Contest.

## Dependencies
- Docker
- A web browser

## Usage
1. Run `docker.sh` to start the NGINX container.
2. Open `eurovision.html` in your browser.
3. Click the button to transition between an adjacency matrix and a bar chart.

## Adjacency Matrix
![Adjacency matrix](/assignment_3/eurovision/images/adjacency_matrix.png)

## Bar Chart
![Bar chart](/assignment_3/eurovision/images/bar_chart.png)
