import java.util.*;

public static final int PAGE_WIDTH = 1337;
public static final int PAGE_HEIGHT = 905;
public static final int X_MARGIN_1 = 50;
public static final int Y_MARGIN_1 = 100;
public static final int X_MARGIN_2 = X_MARGIN_1 + 50;
public static final int Y_MARGIN_2 = Y_MARGIN_1 + 50;
public static final int GRAPH_WIDTH = PAGE_WIDTH - 2 * X_MARGIN_1;
public static final int GRAPH_HEIGHT = PAGE_HEIGHT - 2 * Y_MARGIN_1;

ArrayList<City> cities = new ArrayList<City>();
ArrayList<Army> army1 = new ArrayList<Army>();
ArrayList<Army> army2 = new ArrayList<Army>();
ArrayList<Army> army3 = new ArrayList<Army>();
ArrayList<Temperature> temperatures = new ArrayList<Temperature>();
color red = color(255, 0, 0, 255);
color blue = color(0, 0, 255, 255);

class City {
  private float longitude, latitude, x, y;
  private String name;

  City(float longitude, float latitude, String name) {
    this.longitude = longitude;
    this.latitude = latitude;
    this.name = name;
    this.x = map(longitude, 24, 37.6, X_MARGIN_2, PAGE_WIDTH - X_MARGIN_2);
    this.y = map(-1 * latitude, -55.8, -53.9, Y_MARGIN_2, PAGE_HEIGHT - Y_MARGIN_2);
  }

  public float getX() {
    return this.x;
  }

  public float getY() {
    return this.y;
  }

  public void render() {
    fill(255,0,0,255);
    ellipse(x, y, 20, 20);
    fill(255,255,255,255);
    textSize(20);
    text(name, x - name.length() * 5, y + 30);
  }
}

class Army {
  private float longitude, latitude, x, y;
  private float size;
  private boolean advancing;

  Army(float longitude, float latitude, float size, boolean advancing) {
    this.longitude = longitude;
    this.latitude = latitude;
    this.size = size;
    this.advancing = advancing;
    this.x = map(longitude, 24, 37.6, X_MARGIN_2, PAGE_WIDTH - X_MARGIN_2);
    this.y = map(-1 * latitude, -55.8, -53.9, Y_MARGIN_2, PAGE_HEIGHT - Y_MARGIN_2);
  }

  public float getX() {
    return this.x;
  }

  public float getY() {
    return this.y;
  }

  public float getSize() {
    return size;
  }

  public boolean getAdvancing() {
    return advancing;
  }
}

class Temperature {
  private float longitude;
  private float x;
  private int temperature;
  private int days;
  private String month;
  private int day;

  Temperature(float longitude, int temperature, int size, String month, int day) {
    this.longitude = longitude;
    this.x = map(longitude, 24, 37.6, X_MARGIN_2, PAGE_WIDTH - X_MARGIN_2);
    this.temperature = temperature;
    this.days = days;
    this.month = month;
    this.day = day;
  }

  public float getTemperature() {
    return this.temperature;
  }

  public float getX() {
    return this.x;
  }
}

void setGradient(int x, int y, float w, float h, int t1, int t2 ) {
  noFill();
  for (int i = x; i <= x+w; i++) {
    float t = map(i, x, x+w, t1, t2);
    float inter = map(t, -30, 30, 0, 1);
    color c = lerpColor(blue, red, inter);
    stroke(c);
    line(i, y, i, y+h);
  }
}

float sine(float x1, float y1, float x2, float y2) {
  float opposite = y1 - y2;
  float hypotenuse = dist(x1, y1, x2, y2);
  return opposite / hypotenuse;
}

float cosine(float x1, float y1, float x2, float y2) {
  float adjacent = x1 - x2;
  float hypotenuse = dist(x1, y1, x2, y2);
  return adjacent / hypotenuse;
}

void settings() {
  size(PAGE_WIDTH,PAGE_HEIGHT);
}

void setup() {
  background(255,255,255);
  noLoop();

  Table cityData = loadTable("cities.csv", "header");
  Table temperatureData = loadTable("temperatures.csv", "header");
  Table survivorData = loadTable("survivors.csv", "header");

  for (TableRow row : cityData.rows()) {
    float longitude = row.getFloat("LONC");
    float latitude = row.getFloat("LATC");
    String name = row.getString("CITY");
    cities.add(new City(longitude, latitude, name));
  }
  for (TableRow row : survivorData.rows()) {
    float longitude = row.getFloat("LONP");
    float latitude = row.getFloat("LATP");
    int size = row.getInt("SURV");
    boolean advancing = row.getString("DIR").equals("A");
    if(row.getInt("DIV") == 1)
      army1.add(new Army(longitude, latitude, size, advancing));
    else if(row.getInt("DIV") == 2)
      army2.add(new Army(longitude, latitude, size, advancing));
    else if(row.getInt("DIV") == 3)
      army3.add(new Army(longitude, latitude, size, advancing));
  }
  for (TableRow row : temperatureData.rows()) {
    float longitude = row.getFloat("LONT");
    int temperature = row.getInt("TEMP");
    int days = row.getInt("TEMP");
    String month = row.getString("MON");
    int day = row.getInt("DAY");
    temperatures.add(new Temperature(longitude, temperature, days, month, day));
  }
}

void renderArmy(ArrayList<Army> army) {
  for(int i = 0; i < army.size() - 1; i++) {
    if(army.get(i).getAdvancing())
      fill(100,100,100,255);
    else
      fill(0,0,0,255);
    float xa = army.get(i).getX();
    float ya = army.get(i).getY();
    float xb = army.get(i+1).getX();
    float yb = army.get(i+1).getY();
    float widtha = army.get(i).getSize()/4000;
    float widthb = army.get(i+1).getSize()/4000;
    float x1 = xa+widtha*sine(xa,ya,xb,yb);
    float y1 = ya-widtha*cosine(xa,ya,xb,yb);
    float x2 = xa-widtha*sine(xa,ya,xb,yb);
    float y2 = ya+widtha*cosine(xa,ya,xb,yb);
    float x3 = xb-widthb*sine(xa,ya,xb,yb);
    float y3 = yb+widthb*cosine(xa,ya,xb,yb);
    float x4 = xb+widthb*sine(xa,ya,xb,yb);
    float y4 = yb-widthb*cosine(xa,ya,xb,yb);
    quad(x1, y1, x2, y2 ,x3, y3, x4, y4);
    ellipse(xb, yb, widthb*2, widthb*2);
  }
}

void renderTemperatures() {
  setGradient(X_MARGIN_1, Y_MARGIN_1, temperatures.get(temperatures.size() - 1).getX() - X_MARGIN_1, GRAPH_HEIGHT, 0, -26);
  for(int i = temperatures.size() - 2; i >= 0; i--) {
    int x1 = (int)temperatures.get(i+1).getX();
    int x2 = (int)temperatures.get(i).getX();
    int t1 = (int)temperatures.get(i+1).getTemperature();
    int t2 = (int)temperatures.get(i).getTemperature();
    setGradient(x1, Y_MARGIN_1, x2 - x1, GRAPH_HEIGHT, t1, t2);
  }
  setGradient((int)temperatures.get(0).getX(), Y_MARGIN_1, PAGE_WIDTH - X_MARGIN_1 - temperatures.get(0).getX(), GRAPH_HEIGHT, 0, 0);
}

void renderLegend() {
  noStroke();
  fill(100,100,100,255);
  rect(25, 25, 100, 50);
  fill(0,0,0,255);
  rect(250, 25, 100, 50);
  setGradient(475, 25, 100, 50, -30, 30);
  fill(255,0,0,255);
  noStroke();
  ellipse(975, 50, 50, 50);
  fill(0,0,0,255);
  textSize(20);
  text("Advancing", 150, 60);
  text("Retreating", 375, 60);
  text("Retreating Temperature (-30°C to 30°C)", 600, 60);
  text("City", 1025, 60);
}

void draw() {
  renderTemperatures();
  renderLegend();
  noStroke();
  renderArmy(army1);
  renderArmy(army2);
  renderArmy(army3);
  cities.forEach((city) -> city.render());
  save("minard.png");
}
