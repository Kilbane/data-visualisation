import java.util.*;

public static final int PAGE_WIDTH = 1337;
public static final int PAGE_HEIGHT = 905;
public static final int X_MARGIN_1 = 100;
public static final int X_MARGIN_2 = X_MARGIN_1 + 50;
public static final int Y_MARGIN_1 = 100;
public static final int Y_MARGIN_2 = Y_MARGIN_1 + 50;
public static final int GRAPH_WIDTH_1 = PAGE_WIDTH - 2 * X_MARGIN_1;
public static final int GRAPH_HEIGHT_1 = PAGE_HEIGHT - 2 * Y_MARGIN_1;

ArrayList<Country> countries = new ArrayList<Country>();
LinkedHashMap<String, Integer> continentToColour = new LinkedHashMap<String, Integer>();
LinkedHashMap<String, Integer> continentToShape = new LinkedHashMap<String, Integer>();
LinkedHashMap<String, Integer> continentToOffset = new LinkedHashMap<String, Integer>();
LinkedHashMap<String, Integer> populationToSize = new LinkedHashMap<String, Integer>();
LinkedHashMap<String, Integer> populationToBrightness = new LinkedHashMap<String, Integer>();
LinkedHashMap<String, Integer> populationToAngle = new LinkedHashMap<String, Integer>();

int minYear = Integer.MAX_VALUE;
int maxYear = Integer.MIN_VALUE;
float minLifeExpectancy = Integer.MAX_VALUE;
float maxLifeExpectancy = Integer.MIN_VALUE;
float minPopulation = Integer.MAX_VALUE;
float maxPopulation = Integer.MIN_VALUE;
float minGdpPerCapita = Integer.MAX_VALUE;
float maxGdpPerCapita = Integer.MIN_VALUE;
float minGdp = Integer.MAX_VALUE;
float maxGdp = Integer.MIN_VALUE;

class Country {
  private String name;
  private String continent;
  private int year;
  private float lifeExpectancy;
  private int population;
  private float gdpPerCapita;
  private float gdp;
  private float x;
  private float y;

  Country(String name, String continent, int year, float lifeExpectancy, int population, float gdpPerCapita) {
    this.name = name;
    this.continent = continent;
    this.year = year;
    this.lifeExpectancy = lifeExpectancy;
    this.population = population;
    this.gdpPerCapita = gdpPerCapita;
    this.gdp = (float)Math.log10(gdpPerCapita * population);
  }

  public void render(int variant) {
    if (variant == 0) {
      this.x = map(year, minYear, maxYear, X_MARGIN_2, PAGE_WIDTH - X_MARGIN_2);
      this.y = map(-1 * lifeExpectancy, -1 * maxLifeExpectancy, -1 * minLifeExpectancy, Y_MARGIN_2, PAGE_HEIGHT - Y_MARGIN_2);
      new Encoding(x, y, 10, color(0,0,255,100), 1).render();
    }
    else if (variant == 1) {
      this.x = map(year, minYear, maxYear, X_MARGIN_2, PAGE_WIDTH - X_MARGIN_2);
      this.y = map(-1 * lifeExpectancy, -1 * maxLifeExpectancy, -1 * minLifeExpectancy, Y_MARGIN_2, PAGE_HEIGHT - Y_MARGIN_2);
      new Encoding(x, y, 10, color(0,0,255,100), continentToShape.get(continent)).render();
    }
    else if (variant == 2) {
      this.x = map(year, minYear, maxYear, X_MARGIN_2, PAGE_WIDTH - X_MARGIN_2);
      this.y = map(-1 * lifeExpectancy, -1 * maxLifeExpectancy, -1 * minLifeExpectancy, Y_MARGIN_2, PAGE_HEIGHT - Y_MARGIN_2);
      new Encoding(x, y, 10, continentToColour.get(continent), 1).render();
    }
    else if (variant == 3) {
      this.x = map(year, minYear, maxYear, X_MARGIN_2, PAGE_WIDTH - X_MARGIN_2);
      this.y = map(-1 * lifeExpectancy, -1 * maxLifeExpectancy, -1 * minLifeExpectancy, Y_MARGIN_2, PAGE_HEIGHT - Y_MARGIN_2);
      new Encoding(x + continentToOffset.get(continent), y, 10, color(0,0,255,100), 1).render();
    }
    else if (variant == 4) {
      this.x = map(gdp, minGdp, maxGdp, X_MARGIN_2, PAGE_WIDTH - X_MARGIN_2);
      this.y = map(-1 * lifeExpectancy, -1 * maxLifeExpectancy, -1 * minLifeExpectancy, Y_MARGIN_2, PAGE_HEIGHT - Y_MARGIN_2);
      new Encoding(x, y, 10, color(0,0,255,100), 1).render();
    }
    else if (variant == 5) {
      this.x = map(gdp, minGdp, maxGdp, X_MARGIN_2, PAGE_WIDTH - X_MARGIN_2);
      this.y = map(-1 * lifeExpectancy, -1 * maxLifeExpectancy, -1 * minLifeExpectancy, Y_MARGIN_2, PAGE_HEIGHT - Y_MARGIN_2);
      new Encoding(x, y, (float)(Math.log10(population) * 2 - 5), color(0,0,255,100), 1).render();
    }
    else if (variant == 6) {
      this.x = map(gdp, minGdp, maxGdp, X_MARGIN_2, PAGE_WIDTH - X_MARGIN_2);
      this.y = map(-1 * lifeExpectancy, -1 * maxLifeExpectancy, -1 * minLifeExpectancy, Y_MARGIN_2, PAGE_HEIGHT - Y_MARGIN_2);
      new Encoding(x, y, 10, color(map((float)Math.log10(population), (float)Math.log10(minPopulation), (float)Math.log10(maxPopulation), 0, 200)), 1).render();
    }
    else if (variant == 7) {
      this.x = map(gdp, minGdp, maxGdp, X_MARGIN_2, PAGE_WIDTH - X_MARGIN_2);
      this.y = map(-1 * lifeExpectancy, -1 * maxLifeExpectancy, -1 * minLifeExpectancy, Y_MARGIN_2, PAGE_HEIGHT - Y_MARGIN_2);
      pushMatrix();
      translate(this.x, this.y);
      rotate(radians(map((float)Math.log10(population), (float)Math.log10(minPopulation), (float)Math.log10(maxPopulation), 0, 180)));
      new Encoding(0, 0, 10, color(0,0,255,100), 6).render();
      popMatrix();
    }
  }
}

class Encoding {
  private float x;
  private float y;
  private float size;
  private color c;
  private int shape;

  Encoding(float x, float y, float size, color c, int shape) {
    this.x = x;
    this.y = y;
    this.size = size;
    this.c = c;
    this.shape = shape;
  }

  public void render() {
    fill(c);
    if(shape == 1)
      circle(x, y, size);
    else if(shape == 2)
      arc(x, y, size, size, 0, PI);
    else if(shape == 3)
      arc(x, y, size, size, PI, TWO_PI);
    else if(shape == 4)
      triangle(x - size / 2, y + size / 2, x + size / 2, y + size / 2, x, y - size / 2);
    else if(shape == 5)
      square(x - size / 2, y - size / 2, size);
    else if(shape == 6)
      triangle(x - size / 2, y + size, x + size / 2, y + size, x, y - size);
  }
}

void settings() {
  size(PAGE_WIDTH,PAGE_HEIGHT);
}

void setup() {
  background(255,255,255);
  noLoop();

  Table gapminder = loadTable("gapminder.csv", "header");

  for (TableRow row : gapminder.rows()) {
    String name = row.getString("country");
    String continent = row.getString("continent");
    int year = row.getInt("year");
    float lifeExpectancy = row.getFloat("lifeExp");
    int population = row.getInt("pop");
    float gdpPerCapita = row.getFloat("gdpPercap");
    float gdp = (float)Math.log10(gdpPerCapita * population);
    countries.add(new Country(name, continent, year, lifeExpectancy, population, gdpPerCapita));
    minYear = min(year, minYear);
    maxYear = max(year, maxYear);
    minLifeExpectancy = min(lifeExpectancy, minLifeExpectancy);
    maxLifeExpectancy = max(lifeExpectancy, maxLifeExpectancy);
    minPopulation = min(population, minPopulation);
    maxPopulation = max(population, maxPopulation);
    minGdpPerCapita = min(gdpPerCapita, minGdpPerCapita);
    maxGdpPerCapita = max(gdpPerCapita, maxGdpPerCapita);
    minGdp = min(gdp, minGdp);
    maxGdp = max(gdp, maxGdp);
  }

  continentToColour.put("Americas", color(255,0,0,255));
  continentToColour.put("Europe", color(0,255,0,255));
  continentToColour.put("Africa", color(0,0,255,255));
  continentToColour.put("Asia", color(255,255,0,255));
  continentToColour.put("Oceania", color(255,0,255,255));

  continentToShape.put("Americas", 1);
  continentToShape.put("Europe", 2);
  continentToShape.put("Africa", 3);
  continentToShape.put("Asia", 4);
  continentToShape.put("Oceania", 5);

  continentToOffset.put("Americas", -20);
  continentToOffset.put("Europe", -10);
  continentToOffset.put("Africa", 0);
  continentToOffset.put("Asia", 10);
  continentToOffset.put("Oceania", 20);

  populationToSize.put("1 M", 7);
  populationToSize.put("10 M", 9);
  populationToSize.put("100 M", 11);
  populationToSize.put("1 B", 13);

  populationToAngle.put("1 M", (int)map(6, (float)Math.log10(minPopulation), (float)Math.log10(maxPopulation), 0, 180));
  populationToAngle.put("10 M", (int)map(7, (float)Math.log10(minPopulation), (float)Math.log10(maxPopulation), 0, 180));
  populationToAngle.put("100 M", (int)map(8, (float)Math.log10(minPopulation), (float)Math.log10(maxPopulation), 0, 180));
  populationToAngle.put("1 B", (int)map(9, (float)Math.log10(minPopulation), (float)Math.log10(maxPopulation), 0, 180));

  populationToBrightness.put("1 M", color(map(6, (float)Math.log10(minPopulation), (float)Math.log10(maxPopulation), 0, 200)));
  populationToBrightness.put("10 M", color(map(7, (float)Math.log10(minPopulation), (float)Math.log10(maxPopulation), 0, 200)));
  populationToBrightness.put("100 M", color(map(8, (float)Math.log10(minPopulation), (float)Math.log10(maxPopulation), 0, 200)));
  populationToBrightness.put("1 B", color(map(9, (float)Math.log10(minPopulation), (float)Math.log10(maxPopulation), 0, 200)));
}

void drawLegend(float x, float y, float w, float h, LinkedHashMap<String,Integer> map, String encoding) {
  fill(255,255,255,255);
  rect(x, y, w, h);
  noStroke();
  fill(0,0,0,255);
  int textSize = 20;
  textSize(textSize);
  Object[] objects = map.keySet().toArray();
  String strings[] = new String[objects.length];
  for(int i = 0; i < objects.length; i++) {
    strings[i] = objects[i].toString();
  }
  float interval = (w / (strings.length + 1));
  for(int i = 0; i < strings.length; i++) {
    fill(0,0,0,255);
    String string = strings[i];
    text(string, (i + 1) * interval - (string.length()/4 * textSize), y + textSize);
    if(encoding.equals("None"))
      new Encoding((i + 1) * interval, h / 2, 10, color(0,0,255,100), 1).render();
    else if(encoding.equals("Shape"))
      new Encoding((i + 1) * interval, h / 2, 10, color(0,0,255,100), map.get(string)).render();
    else if (encoding.equals("Colour"))
      new Encoding((i + 1) * interval, h / 2, 10, map.get(string), 1).render();
    else if (encoding.equals("Offset"))
      new Encoding((i + 1) * interval + map.get(string), h / 2, 10, color(0,0,255,100), 1).render();
    else if (encoding.equals("Size"))
      new Encoding((i + 1) * interval, h / 2, map.get(string), color(0,0,255,100), 1).render();
    else if (encoding.equals("Brightness"))
      new Encoding((i + 1) * interval, h / 2, 10, map.get(string), 1).render();
    else if (encoding.equals("Angle")) {
      pushMatrix();
      translate((i + 1) * interval, h / 2);
      rotate(radians(map.get(string)));
      new Encoding(0, 0, 10, color(0,0,255,100), 6).render();
      popMatrix();
    }
  }
}

void drawAxes(float xMin, float xMax, int xIntervals, float yMin, float yMax, int yIntervals) {
  fill(0,0,0,255);
  int textSize = 20;
  textSize(textSize);
  int xInterval = round((xMax - xMin) / (xIntervals - 1));
  int yInterval = round((yMax - yMin) / (yIntervals - 1));
  for(int i = round(xMin); i <= round(xMax); i += xInterval) {
    text(i, map(i, xMin, xMax, X_MARGIN_2, PAGE_WIDTH - X_MARGIN_2) - String.valueOf(i).length() / 4 * textSize, PAGE_HEIGHT - Y_MARGIN_1 + 30);
  }
  for(int i = round(yMin); i <= round(yMax); i += yInterval) {
    text(i, X_MARGIN_1 - 30, map(-1 * i, -1 * yMax, -1 * yMin, Y_MARGIN_2, PAGE_HEIGHT - Y_MARGIN_2) + textSize / 2);
  }
}

void createAxis(float min, float max, int intervals, boolean x) {
  fill(0,0,0,255);
  int textSize = 20;
  int interval = round((max - min) / (intervals - 1));
  int axis[] = new int[intervals];
  int value = round(min);
  for(int i = 0; i < intervals; i++) {
    if(x) {
      text(value, map(i, 0, intervals, X_MARGIN_2, PAGE_WIDTH - X_MARGIN_2) - String.valueOf(value).length() / 4 * textSize, PAGE_HEIGHT - Y_MARGIN_1 + 30);
    }
    else {
      text(value, X_MARGIN_1 - 30, map(-1 * i, -1 * intervals, 0, Y_MARGIN_2, PAGE_HEIGHT - Y_MARGIN_2) + textSize / 2);
    }
    value += interval;
  }
}

void createLogAxis(float min, int intervals, boolean x) {
  fill(0,0,0,255);
  int textSize = 20;
  for(int i = 0; i <= intervals; i++) {
    int value = (int)(pow(10, i + min)/1000000);
    if(x) {
      text(value, map(i, 0, intervals, X_MARGIN_2, PAGE_WIDTH - X_MARGIN_2) - String.valueOf(value).length() / 4 * textSize, PAGE_HEIGHT - Y_MARGIN_1 + 30);
    }
    else {
      text(value, X_MARGIN_1 - 30, map(-1 * i, -1 * intervals, 0, X_MARGIN_2, PAGE_WIDTH - X_MARGIN_2) + textSize / 2);
    }
  }
}

void label(String xLabel, String yLabel, String title) {
  fill(0,0,0,255);
  text(xLabel, PAGE_WIDTH/2 - (xLabel.length()/4) * 20, PAGE_HEIGHT - 40);
  pushMatrix();
  translate(40, PAGE_HEIGHT/2 + (yLabel.length()/4) * 20);
  rotate(radians(270));
  text(yLabel, 0, 0);
  popMatrix();
  textSize(40);
  text(title, PAGE_WIDTH/2 - (title.length()/4) * 40, 50);
}

void drawGraphBackground() {
  stroke(0,0,0,255);
  fill(255,255,255,255);
  rect(X_MARGIN_1, Y_MARGIN_1, GRAPH_WIDTH_1, GRAPH_HEIGHT_1);
}

void reset() {
  noStroke();
  fill(255,255,255,255);
  rect(0, 0, PAGE_WIDTH, PAGE_HEIGHT);
}

void draw() {
  // Part 1 Variant 0
  drawGraphBackground();
  drawAxes(minYear, maxYear, 12, minLifeExpectancy, maxLifeExpectancy, 15);
  label("Year", "Life Expectancy", "Life Expectancy 1957 - 2007");
  noStroke();
  countries.forEach((country) -> country.render(0));
  save("part_1_variant_0.png");
  // Part 1 Variant 1
  drawGraphBackground();
  drawLegend(0, 0, 400, Y_MARGIN_1, continentToShape, "Shape");
  noStroke();
  countries.forEach((country) -> country.render(1));
  save("part_1_variant_1.png");
  // Part 1 Variant 2
  drawGraphBackground();
  drawLegend(0, 0, 400, Y_MARGIN_1, continentToColour, "Colour");
  noStroke();
  countries.forEach((country) -> country.render(2));
  save("part_1_variant_2.png");
  // Part 1 Variant 3
  drawGraphBackground();
  drawLegend(0, 0, 400, Y_MARGIN_1, continentToOffset, "Offset");
  noStroke();
  countries.forEach((country) -> country.render(3));
  save("part_1_variant_3.png");
  // Part 2 Variant 0
  reset();
  drawGraphBackground();
  createLogAxis(8, 5, true);
  createAxis(minLifeExpectancy, maxLifeExpectancy, 15, false);
  label("GDP (in millions)", "Life Expectancy", "Life Expectancy vs GDP");
  noStroke();
  countries.forEach((country) -> country.render(4));
  save("part_2_variant_0.png");
  // Part 2 Variant 1
  drawGraphBackground();
  drawLegend(0, 0, 400, Y_MARGIN_1, populationToSize, "Size");
  noStroke();
  countries.forEach((country) -> country.render(5));
  save("part_2_variant_1.png");
  // Part 2 Variant 2
  drawGraphBackground();
  drawLegend(0, 0, 400, Y_MARGIN_1, populationToBrightness, "Brightness");
  noStroke();
  countries.forEach((country) -> country.render(6));
  save("part_2_variant_2.png");
  // Part 2 Variant 3
  drawGraphBackground();
  drawLegend(0, 0, 400, Y_MARGIN_1, populationToAngle, "Angle");
  noStroke();
  countries.forEach((country) -> country.render(7));
  save("part_2_variant_3.png");
}
